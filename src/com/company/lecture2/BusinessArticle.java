package com.company.lecture2;

/**
 * Created by wenceslaus on 13.12.16.
 */
public class BusinessArticle extends Article {

    // Создать базовый класс "Транспорт", в котором есть параметр "максимальная скорость передвижения"
    // default maxSpeed == 200;
    // и метод maxSpeed()

    // Создать класс наследник "Автобус", который расширяет базовый класс добавляя параметр
    // вместительность и ограничивает скорость до 70 км/ч

    String author;

    public BusinessArticle(String title, String author) {
        super(title);
        this.author = author;
    }

    @Override
    public void print() {
        System.out.println(author + " " + title);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        BusinessArticle that = (BusinessArticle) o;

        return !(author != null ? !author.equals(that.author) : that.author != null);

    }

    @Override
    public String toString() {
        return "author -> \"" + author + "\"";
    }

    @Override
    public int hashCode() {
        return author.hashCode();
    }
}
