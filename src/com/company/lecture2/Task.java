package com.company.lecture2;

/**
 * Created by wenceslaus on 13.12.16.
 */
public class Task {

    public static void main(String[] args) {
        Article article = new Article("A1");
        BusinessArticle businessArticle = new BusinessArticle("BA1", "author");

        article.print();
//        article.
//
//        businessArticle.print();
//
//        System.out.println(businessArticle.toString());
//
//        Object obj = new BusinessArticle("BA1", "author");
//        Article article1 = (Article) obj;
//        BusinessArticle businessArticle1 = (BusinessArticle) article1;

        Literature[] literatures = new Literature[3];

        literatures[0] = new Book();
        literatures[1] = new Magazine();
        literatures[2] = new Literature();

        for (Literature literature : literatures) {
            System.out.println(literature.getInfo());
        }
    }
}
