package com.company.lecture2;

/**
 * Created by wenceslaus on 13.12.16.
 */
public class Article {

    String title;

    public Article(String title) {
        this.title = title;
    }

    public void print() {
        System.out.println(title);
    }

    public String getTitle() {
        return title;
    }
}
