package com.company.lecture4;

/**
 * Created by wenceslaus on 17.12.16.
 */
public class Circle extends Figure{
    int radius;

    public Circle(String name, int radius) {
        super(name);
        this.radius = radius;
    }

    @Override
    public double getSquare() {
        return Math.PI * radius * radius;
    }
}
