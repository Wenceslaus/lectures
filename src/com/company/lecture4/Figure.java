package com.company.lecture4;

/**
 * Created by wenceslaus on 17.12.16.
 */
public abstract class Figure {
    String name;

    public Figure(String name) {
        this.name = name;
    }

    public abstract double getSquare();
}
