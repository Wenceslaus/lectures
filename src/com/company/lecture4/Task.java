package com.company.lecture4;

/**
 * Created by wenceslaus on 17.12.16.
 */
public class Task {

    public static void main(String[] args) {
        Figure[] figures = new Figure[3];

        figures[0] = new Circle("c", 5);
        figures[1] = new Foursquare("f", 6);
        figures[2] = new Triangle("t");

        for (Figure figure : figures) {
            if (figure instanceof IPoligon) {
//                figure.getSideNumber();
                IPoligon poligon = (IPoligon) figure;
                System.out.println(figure.name + " " + poligon.getSideNumber());
            }
        }

//        Circle circle = new Circle("c1", 4);
//        circle.

//        for (int i = 0; i < figures.length; i++) {
//            System.out.println(figures[i].name + " " + figures[i].getSquare());
//        }
//
//        for (Figure figure : figures) {
//            System.out.println(figure.name + " " + figure.getSquare());
//        }


        int a = 6;
        Figure figure = new Figure("f2") {
            @Override
            public double getSquare() {
                return a * 6;
            }
        };
    }
}
