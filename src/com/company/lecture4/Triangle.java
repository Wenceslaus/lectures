package com.company.lecture4;

/**
 * Created by wenceslaus on 17.12.16.
 */
public class Triangle extends Figure implements IPoligon{

    public Triangle(String name) {
        super(name);
    }

    @Override
    public double getSquare() {
        return 1;
    }

    @Override
    public int getSideNumber() {
        return 3;
    }
}
