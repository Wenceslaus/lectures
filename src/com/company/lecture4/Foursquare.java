package com.company.lecture4;

/**
 * Created by wenceslaus on 17.12.16.
 */
public class Foursquare extends Figure implements IPoligon {

    int side;

    public Foursquare(String name, int side) {
        super(name);
        this.side = side;
    }

    @Override
    public double getSquare() {
        return side * side;
    }

    @Override
    public int getSideNumber() {
        return 4;
    }
}
