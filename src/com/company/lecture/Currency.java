package com.company.lecture;

/**
 * Created by wenceslaus on 10.12.16.
 */
public class Currency {

    public String name;
    public double buy;
    public double sell;

    public Currency(String name, double buy, double sell) {
        this.name = name;
        this.buy = buy;
        this.sell = sell;
        Comparable c = new Comparable() {
            @Override
            public int compareTo(Object o) {
                return 0;
            }
        };
    }

}
