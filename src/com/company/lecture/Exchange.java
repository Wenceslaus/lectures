package com.company.lecture;

/**
 * Created by wenceslaus on 10.12.16.
 */
public class Exchange {
    public String city;
    public String address;
    public Currency[] currencies;

    public Currency getCurrency(String name) {
        for (Currency currency : currencies) {
            if (currency.name.equalsIgnoreCase(name)) {
                return currency;
            }
        }
        return null;
    }

    public double convert(String name, double sum) {
        Currency currency = getCurrency(name);
        return sum / currency.buy;
    }
}
