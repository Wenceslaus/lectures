package com.company.lecture;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

/**
 * Created by wenceslaus on 10.12.16.
 */
public class TaskLecture {

    static Exchange[] exchanges = new Exchange[3];

    public static void main(String[] args) throws IOException {
        initExchanges();

        String curr = "usd";

        Exchange bestExchange = exchanges[0];
        for (int i = 0; i < exchanges.length; i++) {
            if (exchanges[i].getCurrency(curr).buy < bestExchange.getCurrency(curr).buy) {
                bestExchange = exchanges[i];
            }
        }

        System.out.println(bestExchange.city + " " + bestExchange.convert(curr, 5000));
        inputText();
    }

    private static void initExchanges() {
        for (int i = 0; i < exchanges.length; i++) {
            Exchange newExchange = new Exchange();
            newExchange.city = "City# " + (i + 1);
            newExchange.address = "Address# " + (i + 1);
            newExchange.currencies = initCurrencies(i);
            exchanges[i] = newExchange;
        }
    }

    private static Currency[] initCurrencies(int i) {
        Currency[] currencies = new Currency[3];
        currencies[0] = new Currency("usd", 27 + i, 28 + i);
        currencies[1] = new Currency("eur", 29 + i, 30 + i);
        currencies[2] = new Currency("gbp", 35 + i, 40 + i);
        return currencies;
    }
    private static BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));
    private static List<String> inputText() throws IOException {
        List<String> mySentences = new ArrayList<>();
        System.out.println("Input a sentences and press <Enter>: ");
        final String sents = bufferedReader.readLine();
        final String separator = "[.!?]";
        int sentenceCount = 0;
        for (int i = 0; i < sents.length(); i++) {
            if (separator.indexOf(sents.charAt(i)) != -1 ) {
                sentenceCount++;
            }
        }
        mySentences.addAll(Arrays.asList(sents.split(separator)));

        // minimal and maximal length sentence
        StringBuilder sb1 = new StringBuilder(30);
        StringBuilder sb2 = new StringBuilder(30);

        for (String mySentence : mySentences) {
            int min = mySentences.get(0).length();
            int max = mySentences.get(0).length();
            if (min > mySentence.length()) {
                sb1.append(mySentence);
//                String minimalSentence = mySentence;
            } else if (max < mySentence.length()) {
                sb2.append(mySentence);
//                String maximalSentence = mySentence;
            }
        }

        System.out.printf("Sentences amount: %s", sentenceCount + "\n");
        System.out.printf("Sentence with minimum characters %s " +
                "and sentence with maximum characters %s", sb1.toString(), sb2.toString());
        return mySentences;
    }


}
