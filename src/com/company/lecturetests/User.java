package com.company.lecturetests;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by wenceslaus on 27.12.16.
 */
public class User {

    public static final Pattern VALID_EMAIL_ADDRESS_REGEX =
            Pattern.compile("^[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]{2,6}$", Pattern.CASE_INSENSITIVE);

    private String name;
    private String email;
    private int age;

    public User(String name, String email, int age) {
        this.name = name;
        this.email = email;
        this.age = age;
    }

    public boolean isNameValid() {
        return name != null && name.length() >= 2 && name.length() < 30;
    }

    public boolean isEmailValid() {
        Matcher matcher = VALID_EMAIL_ADDRESS_REGEX.matcher(email);
        return matcher.find();
    }

    public boolean isAgeValid() {
        return age > 0 && age < 120;
    }

    public String getName() {
        return name;
    }

    public String getEmail() {
        return email;
    }

    public int getAge() {
        return age;
    }
}
