package com.company.lecturetests;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

/**
 * Created by wenceslaus on 27.12.16.
 */
public class UserTest {

    @Test
    public void test() {
        List<User> validUsers = new ArrayList<>();
        validUsers.add(new User("Jhon", "jhon@gmail.com", 24));
        validUsers.add(new User("Vasya", "vasya@imail.ua", 98));

        List<User> invalidUsers = new ArrayList<>();
        invalidUsers.add(new User("", "zba@gmailcom", 0));
        invalidUsers.add(new User("L", "zbagmail.com", 20000));
        invalidUsers.add(new User(null, "user@gmail.comcomcom", -1));
        invalidUsers.add(new User("qwertyuiopqwertyuiopqwertyuiopqwertyuiopqwertyuiop", "user@gmail.comcomcom", -1));

        for (User user : validUsers) {
            Assert.assertTrue(user.isNameValid());
            Assert.assertTrue(user.isEmailValid());
            Assert.assertTrue(user.isAgeValid());
        }

        for (User invalidUser : invalidUsers) {
            Assert.assertFalse("name: " + invalidUser.getName() + " is correct", invalidUser.isNameValid());
            Assert.assertFalse(invalidUser.isEmailValid());
            Assert.assertFalse(invalidUser.isAgeValid());
        }

    }

    @Before
    public void prepare() {

    }

    @After
    public void finish() {

    }

}