package com.company.lecturetest2;

import com.company.lecturetests.UserTest;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;

/**
 * Created by wenceslaus on 27.12.16.
 */
@RunWith(Suite.class)
@Suite.SuiteClasses({
        CompanyTest.class,
        UserTest.class
})
public class GlobalTest {

}
