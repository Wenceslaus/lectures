package com.company.lecturetest2;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.Calendar;

import static org.junit.Assert.*;

/**
 * Created by wenceslaus on 27.12.16.
 */
public class CompanyTest {

    Company company;

    @Before
    public void init() {
        company = new Company();
    }

    @Test(expected = IllegalArgumentException.class)
    public void testNameIsNull() {
        company.setName(null);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testNameIsEmpty() {
        company.setName("");
    }

    @Test(expected = IllegalArgumentException.class)
    public void testNameIsLong() {
        company.setName("qwertyuioqwertyuiopqwertyuiopqwertyuiopqwertyuiop");
    }

    @Test
    public void testNameValid() {
        company.setName("TheBestCompany");
        Assert.assertEquals("TheBestCompany", company.getName());
    }

    @Test(expected = IllegalArgumentException.class)
    public void testIncorrectYear() {
        company.setYear(Calendar.getInstance().get(Calendar.YEAR) + 20);
    }

    @Test
    public void testCorrectYear() {
        company.setYear(2000);
        Assert.assertNotEquals(2000, company.getYear());
    }

    @After
    public void clear() {
        company = null;
    }

}