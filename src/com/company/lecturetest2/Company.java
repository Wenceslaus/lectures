package com.company.lecturetest2;

import java.util.Calendar;

/**
 * Created by wenceslaus on 27.12.16.
 */
public class Company {

    private String name;
    private int year;

    public Company() {
    }

    public void setName(String name) {
        if (name == null) {
            throw new IllegalArgumentException("Name mustn't be null");
        }

        if (name.length() < 2 || name.length() > 30) {
            throw new IllegalArgumentException("Name be in range 2-30");
        }

        this.name = name;
    }

    public void setYear(int year) {
        if (year < 1991) {
            throw new IllegalArgumentException("Company mustn't be register in USSR");
        }

        if (year > Calendar.getInstance().get(Calendar.YEAR)) {
            throw new IllegalArgumentException("Invalid year");
        }

        this.year = year;
    }

    public String getName() {
        return name;
    }

    public int getYear() {
        return year;
    }
}
