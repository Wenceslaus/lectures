package com.company.lecture5;

/**
 * Created by wenceslaus on 17.12.16.
 */
public class Lombard extends Company implements Credit{

    public Lombard(String name) {
        super(name);
    }

    @Override
    public boolean canGiveCredit() {
        return true;
    }

    @Override
    public double rate() {
        return 70;
    }
}
