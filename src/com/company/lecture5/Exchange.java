package com.company.lecture5;

/**
 * Created by wenceslaus on 17.12.16.
 */
public interface Exchange {

    double convertUsd(int uah);

    double convertEur(int uah);
}
