package com.company.lecture5;

/**
 * Created by wenceslaus on 17.12.16.
 */
public class MMM extends Company implements Debet {

    public MMM(String name) {
        super(name);
    }

    @Override
    public int getDebetRate() {
        return 100;
    }
}
