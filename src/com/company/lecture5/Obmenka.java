package com.company.lecture5;

/**
 * Created by wenceslaus on 17.12.16.
 */
public class Obmenka extends Company implements Exchange{


    public Obmenka(String name) {
        super(name);
    }

    @Override
    public double convertUsd(int uah) {
        return uah / 25;
    }

    @Override
    public double convertEur(int uah) {
        return 0;
    }
}
