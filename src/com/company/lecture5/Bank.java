package com.company.lecture5;

/**
 * Created by wenceslaus on 17.12.16.
 */
public class Bank extends Company implements Exchange, Credit, Debet {

    public Bank(String name) {
        super(name);
    }

    @Override
    public double convertUsd(int uah) {
        return uah / 27;
    }

    @Override
    public double convertEur(int uah) {
        return uah / 29;
    }

    @Override
    public boolean canGiveCredit() {
        return true;
    }

    @Override
    public double rate() {
        return 25;
    }

    @Override
    public int getDebetRate() {
        return 20;
    }
}
