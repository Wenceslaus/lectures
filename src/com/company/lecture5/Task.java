package com.company.lecture5;

/**
 * Created by wenceslaus on 17.12.16.
 */
public class Task {

    public static void main(String[] args) {
        Company[] companies = new Company[4];

        companies[0] = new Bank("Privat");
        companies[1] = new Obmenka("Roza");
        companies[2] = new Lombard("Dars");
        companies[3] = new MMM("Mavrody");



        //best exchange rate
        double usd = 0;
        String bestExchange = "";

        for (Company company : companies) {
            if (company instanceof Exchange) {
                Exchange exchange = (Exchange) company;
                double currentExchange = exchange.convertUsd(5000);
                if (usd == 0 || usd < currentExchange) {
                    usd = currentExchange;
                    bestExchange = company.name;
                }
            }
        }

        System.out.println(bestExchange + " " + usd);




    }
}
