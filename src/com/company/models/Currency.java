package com.company.models;

public class Currency {
    private String name;
    private double sell;
    private double buy;

    public Currency(String name, double sell1, double buy1) {
        this.name = name;
        sell = sell1;
        buy = buy1;
    }
}
