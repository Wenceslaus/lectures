package com.company.test;

import java.sql.Date;
import java.util.*;

/**
 * Created by wenceslaus on 24.12.16.
 */
public class TaskArrays {

    public static void main(String[] args) {

        ClassArray classArray1 = new ClassArray();
        classArray1.addTag("tag1");
        classArray1.addTag("tag2");

//        System.out.println(classArray1.containsTag("tag3"));

        List<String> tags = new ArrayList<>();
        tags.add("t1");
        tags.add("t2");
        tags.add("ta2");
        ClassArray classArray2 = new ClassArray(tags);
//        System.out.println(classArray2.containsTag("t1"));

        System.out.println(classArray1.containsOneOfTags(tags));


        List<ClassArray> list = new ArrayList<>();
        list.add(classArray1);
        list.add(classArray2);

        Collections.sort(list);
        Collections.sort(list, new Comparator<ClassArray>() {
            @Override
            public int compare(ClassArray o1, ClassArray o2) {
                return o2.year - o1.year;
            }
        });

        Collections.sort(list, new SortByYear());

        System.out.println(list);

        HashMap<Integer, String> map = new HashMap<>();
        map.put(1, "1");
        map.put(1, "2");
        map.remove(1);
        map.get(1);
        map.replace(1, "5");
        Set<Integer> set = map.keySet();
        for (Integer integer : set) {
            System.out.println(integer);
        }

        set.add(1);
    }

    static class SortByYear implements Comparator<ClassArray> {

        @Override
        public int compare(ClassArray o1, ClassArray o2) {
            return o2.year - o1.year;
        }
    }

    static class ClassArray implements Comparable<ClassArray>{
        String title;
        Date date;
        int year;

        List<String> tags = new ArrayList<>();

        public ClassArray(List<String> tags) {
            this.tags = tags;
        }

        public ClassArray(String title, Date date) {
            this.title = title;
            this.date = date;
        }

        public ClassArray() {
        }

        public void setTags(List<String> tags) {
            this.tags = tags;
        }

        public boolean containsTag(String tag) {
            return tags.contains(tag);
        }

        public boolean containsOneOfTags(List<String> tags) {
            for (String tag : tags) {
                if (this.tags.contains(tag)) {
                    return true;
                }
            }
            return false;
        }

        @Override
        public String toString() {
            return "ClassArray{" +
                    "year=" + year +
                    ", date=" + date +
                    ", title='" + title + '\'' +
                    '}';
        }

        public void addTag(String tag) {
            tags.add(tag);
        }

        @Override
        public int compareTo(ClassArray o) {
            return year - o.year;
        }
    }

}
