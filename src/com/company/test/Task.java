package com.company.test;

import java.sql.Date;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Locale;

public class Task {
    public static void main(String[] args) {


        Calendar calendar = Calendar.getInstance();
        calendar.set(2009, Calendar.MAY, 10);
        Date date = new Date(calendar.getTimeInMillis());
        Date incorrect = new Date(2009, 4, 10);
//        System.out.println("1: " + date.toString());
//        System.out.println("1: " + date.getYear());

        MyClass myClass = new MyClass(date);
        myClass.printDate();
    }

    static class MyClass {
        Date date;

        public MyClass(Date date) {
            this.date = date;
        }

        boolean equalsByYear(MyClass myClass) {
            Calendar calendar1 = Calendar.getInstance();
            calendar1.setTime(date);
            int year1 = calendar1.get(Calendar.YEAR);

            Calendar calendar2 = Calendar.getInstance();
            calendar2.setTime(myClass.date);
            int year2 = calendar2.get(Calendar.YEAR);
            return year1 == year2;
        }

        void printDate() {
            SimpleDateFormat df = new SimpleDateFormat("dd MMM yy", Locale.US);
            System.out.println(df.format(date));
        }
    }

}
